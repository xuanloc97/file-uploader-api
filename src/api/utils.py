"""
Contain common function to help proccessing
"""
import re
import os
import hashlib
from flask import current_app

from src import config


def hash_md5(data):
    """
    Hash content data
    """
    md5sum = hashlib.md5()
    md5sum.update(str(data).encode('utf-8'))
    return str(md5sum.hexdigest())

def check_file_size_limit(file_data):
    """
    check file limit from file upload. if file > size limit defind will be return -1
    :param fileupload: file data on request post
    :return:
             - : 0 oke
             - : -1 error
    """

    if file_data:
        if len(file_data) <= config.FILE_SIZE:
            return 0
        return  -1
    return -1

def check_file_type(file_name):
    """
    check file type wwith file name

    :param fileobject: fileobject
    :return:
             - : 0 oke
             - : -1 error
    """
    if len(file_name.split(".")) == 2:
        get_type_file = re.findall("[a-zA-Z/-/_]*[/.]([a-zA-Z]*)", file_name)
        try:
            typ = get_type_file[len(get_type_file) - 1]
        except Exception:
            typ = get_type_file[0]

    elif len(file_name.split(".")) > 2:
        typ = file_name.split(".")[-1]
    else:
        typ = file_name

    if typ in  [ type for type in config.FILE_TYPE_ALLOW.keys()]:
        return 0, config.FILE_TYPE_ALLOW[typ]

    return -1, "not found"


def check_signature(md5_content_file, sign_of_client):
    """
    Check the calling client's signature. The purpose of ensuring the integrity of the data sent is not modified during the process
    sender.

    Data will be sent to the server by the client md5(md5(content_file) + SECRET_KEY) in the header
    The server side will receive the data file, regenerate the sign and compare to ensure the data is correct

    :param md5_content_file:
    :param sign_of_client:
    :return:
      - oke : 0
      - fail: -1

    """
    sign = hash_md5(str(md5_content_file + config.SECRET_KEY))
    current_app.logger.debug(sign)
    current_app.logger.debug(sign_of_client)
    if str(sign) == sign_of_client:
        return 0
    return -1

def read_uploaded_file_by_chunk_size(file_object, chunk_size=1024):
    """Reads data from an uploaded file and reads it by chunk size.

    :param file_object: The uploaded file object.
    :param chunk_size: The size of each chunk in bytes.
    Returns:
        A list of bytes, where each byte represents a chunk of data from the file.
    """
    file_data = b""
    current_app.logger.debug(f'file : {file_object}')
    while True:
        data = file_object.read(chunk_size)
        current_app.logger.debug(f'file_data : {data}')
        if not data:
            break
        file_data = file_data + data
    return file_data

def handle_upload_file_disk(file_data, sign_client):
    """
    will save file with new name, name is hash md5 content of data
    :param file_data:
    :param sign_client
    :return:

        - oke     :  0, filename_hash_md5
        - not found:  -2, error
    """
    current_app.logger.debug(sign_client)

    # We should implement check_signature to more security
    # check = check_signature(str(hash_md5(file_data)), sign_client)
    # if check == -1:
    #     raise SignError(sign_client)

    # Here we can upload file to S3 instead of system disk
    try:
        with open(config.RESOURCE_UPLOAD + str(hash_md5(file_data)), 'wb+') as destination:
            current_app.logger.debug(file_data)
            destination.write(file_data)
        destination.close()
        return (0, str(hash_md5(file_data)))

    except Exception as err:
        current_app.logger.debug(err)
        return (-2, str(err))

def handle_delete_file_disk(file_name):
    """
    remove file on disk

    :param dir_file_name:
    :return:
             - : 0 oke
             - : -1 file not found
             - : -2 error (permission deny)
    """
    # Here we can delete file to S3 instead of system disk
    if os.path.exists(config.RESOURCE_UPLOAD + file_name) and os.path.isfile(config.RESOURCE_UPLOAD + file_name):
        try:
            os.remove(config.RESOURCE_UPLOAD + file_name)
            return (0, file_name)
        except OSError as err:
            return (-2, str(err))
    else:
        return (-1, 'file not found')

def handle_read_file_disk(file_name):
    """
    read file on disk

    :param dir_file_name:
    :return:
             - : 0 oke
             - : -1 file not found
             - : -2 error (permission deny)
    """
    # Here we can find a file to S3 instead of system disk
    data = ""
    if os.path.exists(config.RESOURCE_UPLOAD + file_name):
        try:
            with open(config.RESOURCE_UPLOAD + file_name, 'rb') as file_data:
                data = file_data.read()
            file_data.close()
            return 0, data
        except OSError as err:
            return (-2, str(err))
    else:
        return -1, 'file not found'
