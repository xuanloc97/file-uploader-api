"""
Define custom exceptions
"""
class Base(Exception):
    """
    Custom base exception
    """
    def __init__(self, *args, **kwargs):
        self.msg = (list(args)[0:1] + [""])[0]
        super(Exception, self).__init__(*args, **kwargs)

    def __repr__(self):
        return repr(self.msg)

class UnknownTypeException(Base):
    """
    UnknownTypeException
    """
    _def_message = "Unknown type {}."

    def __init__(self, tp, msg=None):
        self._def_message = msg or self._def_message
        msg = self._def_message.format(tp)
        super(UnknownTypeException, self).__init__(msg)

class DataNotReady(Base):
    """
    DataNotReady
    """

class FileDoesNotExist(Base):
    """
    FileDoesNotExist
    """

class SignError(Base):
    """
    SignError
    """

class FileTypeNotAllow(Base):
    """
    FileTypeNotAllow
    """

class FileSizeLimit(Base):
    """
    FileSizeLimit
    """

class ServerError(Base):
    """
    ServerError
    """

class FileNotSync(Base):
    """
    FileNotSync
    """
