"""
File Uploader Server API
Purposes:
● Upload a new file
● Retrieve an uploaded file by name
● Delete an uploaded file by name
● if multiple files have similar contents, reuse the contents
"""
from datetime import datetime

from flask import current_app, Blueprint, Response, request, make_response

from src.api.exceptions import FileDoesNotExist

from src.api.utils import (
    check_file_size_limit,
    check_file_type,
    read_uploaded_file_by_chunk_size,
    handle_delete_file_disk,
    handle_read_file_disk,
    handle_upload_file_disk
)

from src.helpers import respone_status
from src.helpers.response import ResponseAPI
from src.helpers.authentication import token_required

from src.api.models import Files, db


file_uploader_api = Blueprint('file_uploader_api', __name__)


@file_uploader_api.route('/checkhealth', methods=['GET'])
def healthcheck():
    """
    Check healthy for server
    """
    return make_response("OK", 200)


@file_uploader_api.route('/viewfile', methods=['GET'])
@token_required
def view_file():
    """
    View file with method get
    """
    file_name = request.args['file_name']
    status , content_type = check_file_type(file_name)

    # validate file type
    if status == 0:
        # file type allow

        # check file in database
        file_db = Files.query.filter_by(name=file_name).first()
        if file_db is None:
            current_app.logger.info(f'{file_name}: File not found')
            return ResponseAPI(
                respone_code=respone_status.FILE_DOES_NOT_EXIST.code,
                response_message=respone_status.FILE_DOES_NOT_EXIST.message).resp

        # check file on disk
        status, file_content = handle_read_file_disk(file_db.hash)

        # file okie
        if status == 0:
            response = Response(file_content, content_type=str(content_type))
            return response

        # file not found
        if status == -1:
            current_app.logger.error(f'{respone_status.FILE_DOES_NOT_EXIST.message}')
            return ResponseAPI(
                respone_code=respone_status.FILE_DOES_NOT_EXIST.code,
                response_message=respone_status.FILE_DOES_NOT_EXIST.message).resp

        # file error
        if status == -2:
            current_app.logger.error(f'{respone_status.SERVER_ERROR.message}')
            return ResponseAPI(
                respone_code=respone_status.SERVER_ERROR.code,
                response_message=respone_status.SERVER_ERROR.message).resp

    else:
        # file type not allow
        current_app.logger.error(f'{respone_status.FILE_TYPE_NOT_ALLOW.message}')
        return ResponseAPI(
            respone_code=respone_status.FILE_TYPE_NOT_ALLOW.code,
            response_message=respone_status.FILE_TYPE_NOT_ALLOW.message).resp


@file_uploader_api.route('/uploadfile', methods=['POST'])
@token_required
def upload_file():
    """
    Process file data from client
    """
    file_name = ""

    if 'file' not in request.files:
        current_app.logger.error('No file part')
        return make_response('No file part', 404)
    # files = request.files.getlist("file")
    file = request.files['file']

    # Process file
    file_name = file.filename
    file_data = read_uploaded_file_by_chunk_size(file)

    # validate file type
    status, _ = check_file_type(file_name)

    if status == 0:
        # file type allow
        # check file size
        size_limit = check_file_size_limit(file_data)

        # file size in limit
        if size_limit == 0:
            try:
                sign_client = request.environ['HTTP_SIGN_CLIENT']

            except Exception:
                sign_client = ""
            # Handle file date
            status, message = handle_upload_file_disk(file_data, sign_client)

            if status == 0:

                # check file name native have exits
                try:
                    # if record have in database
                    file_db = Files.query.filter_by(name=str(file.filename)).first()

                    if file_db is None:
                        # if name native record not exits and database
                        new_file_db_row = Files(name=str(file.filename), hash=str(message), time_modify=datetime.utcnow())
                        db.session.add(new_file_db_row)
                        db.session.commit()

                        return ResponseAPI(
                            respone_code=respone_status.SUCCESS.code,
                            response_message=respone_status.SUCCESS.message,
                            response_data=new_file_db_row.to_dict()).resp

                    else:
                        current_app.logger.error(str(file_db.name) + " exist", extra={"client_ip": request.environ['REMOTE_ADDR'], "user": None})
                        return ResponseAPI(
                            respone_code=respone_status.FILE_EXIST.code,
                            response_message=respone_status.FILE_EXIST.message,
                            response_data=file_db.to_dict()).resp

                except Exception as err:
                    db.session.rollback()
                    current_app.logger.info(f'Server Error when save file on DB reason: {err}')
                    return ResponseAPI(
                        respone_code=respone_status.SERVER_ERROR.code,
                        response_message=respone_status.SERVER_ERROR.message).resp

            elif status == -1:
                current_app.logger.error(f'{FileDoesNotExist(message)}')
                return ResponseAPI(
                    respone_code=respone_status.FILE_DOES_NOT_EXIST.code,
                    response_message=respone_status.FILE_DOES_NOT_EXIST.message).resp
            else:
                current_app.logger.info('Server Error when posting file')
                return ResponseAPI(
                    respone_code=respone_status.SERVER_ERROR.code,
                    response_message=respone_status.SERVER_ERROR.message,
                    response_data="Server Error when posting file").resp

        else:
            # file size > limit
            current_app.logger.info('File size max limit')
            return ResponseAPI(
                respone_code=respone_status.FILE_SIZE_LIMIT.code,
                response_message=respone_status.FILE_SIZE_LIMIT.message,
                response_data="File size max limit").resp

    # file type not allow
    else:
        current_app.logger.error(f'{respone_status.FILE_TYPE_NOT_ALLOW.message}')
        return ResponseAPI(
            respone_code=respone_status.FILE_TYPE_NOT_ALLOW.code,
            response_message=respone_status.FILE_TYPE_NOT_ALLOW.message).resp


@file_uploader_api.route('/uploadfile', methods=['DELETE'])
@token_required
def delete_file():
    """
    Delete file have saved on system
    """
    total_same_hashs = 0
    file_name = request.args['file_name']
    # validate file type
    status, _ = check_file_type(file_name)

    # file type allow
    if status == 0:
        try:
            # check file in database
            file_db = Files.query.filter_by(name=str(file_name)).first()

            if file_db:
                # Find all files contain same hash
                total_same_hashs = Files.query.filter_by(hash=file_db.hash).count()

                if total_same_hashs == 1:
                    # check file on disk
                    file_status, file_name = handle_delete_file_disk(file_db.hash)
                    # delete on DB
                    db.session.delete(file_db)
                    db.session.commit()
                    # file had deleted
                    if file_status == 0:
                        current_app.logger.info(f'{file_db.hash} + ":" + {file_db.name} has already deleted')
                        return ResponseAPI(
                            respone_code=respone_status.SUCCESS.code,
                            response_message=respone_status.SUCCESS.message,
                            response_data="Removed file " + file_db.name).resp
                    # file not found on disk
                    elif file_status == -1:
                        current_app.logger.info(f'{file_db.name}: File not found on Disk')
                        return ResponseAPI(
                            respone_code=respone_status.FILE_DOES_NOT_EXIST.code,
                            response_message=respone_status.FILE_DOES_NOT_EXIST.message,
                            response_data=file_db.to_dict()).resp

                    # file error
                    elif file_status == -2:
                        current_app.logger.info('Server Error when deleting file')
                        return ResponseAPI(
                            respone_code=respone_status.SERVER_ERROR.code,
                            response_message=respone_status.SERVER_ERROR.message,
                            response_data=file_db.to_dict()).resp

                elif total_same_hashs > 1:
                    # delete on DB
                    db.session.delete(file_db)
                    db.session.commit()
                    current_app.logger.info(f'{file_db.name} is deleted on DB')
                    return ResponseAPI(
                        respone_code=respone_status.SUCCESS.code,
                        response_message=respone_status.SUCCESS.message,
                        response_data=file_name + ": Only remove file on DB ").resp
            else:
                current_app.logger.error(f'{respone_status.FILE_DOES_NOT_EXIST.message}')
                return ResponseAPI(
                    respone_code=respone_status.FILE_DOES_NOT_EXIST.code,
                    response_message=respone_status.FILE_DOES_NOT_EXIST.message).resp

        except Exception as err:
            # Need to rollback transactions uncompleted
            current_app.logger.error(f'Have error when process DB reason: {err}')
            db.session.rollback()
            return ResponseAPI(
                respone_code=respone_status.SERVER_ERROR.code,
                response_message=respone_status.SERVER_ERROR.message).resp

    else:
        # file type not allow
        current_app.logger.error(f'{respone_status.FILE_TYPE_NOT_ALLOW.message}')
        return ResponseAPI(
            respone_code=respone_status.FILE_TYPE_NOT_ALLOW.code,
            response_message=respone_status.FILE_TYPE_NOT_ALLOW.message).resp
