"""
Initialize flask app
"""
import sys
import logging
from flask import Flask

from src import config
from src.api.models import db

from .file_uploader_api import file_uploader_api
from .user_api import user_api

def setupLogging(app_logger, level=logging.DEBUG):
    """
    Custom logging on app
    """
    # Set log level
    if config.ENVIRONMENT in ("production", "prod") :
        level = logging.INFO
    logging.basicConfig(level=level)
    # Define log formater
    formatter = logging.Formatter('%(levelname)-8s:%(module)s: %(lineno)s - message: %(message)s')
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    app_logger.addHandler(handler)

def create_app():
    """
    Init flask app and set configs for app
    """
    app = Flask(__name__)
    app.config['SECRET_KEY'] = config.SECRET_KEY
    app.config['SQLALCHEMY_DATABASE_URI'] = config.DATABASE_URL
    db.init_app(app)

    # Formater logging
    setupLogging(app.logger)

    app.register_blueprint(file_uploader_api, url_prefix='/')
    app.register_blueprint(user_api, url_prefix='/')

    with app.app_context():
        db.create_all()

    # Catch all unxpected exceptions
    @app.errorhandler(Exception)
    def all_exceptoon_handler(error):
        return f"There was an internal server error: {error}", 500

    return app
