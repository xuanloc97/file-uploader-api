"""
All config app
"""
import os

DB_NAME = "database.db"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

## get env variable for container

# set default
if os.environ.get("DATABASE_URL") is None:
    os.environ["DATABASE_URL"] = f'sqlite:///{os.path.join(BASE_DIR, DB_NAME)}'

if os.environ.get("APP_SECRET_KEY") is None:
    os.environ["APP_SECRET_KEY"] = "sdfsfFKLJodsg082343223_"

if os.environ.get("APP_FILE_SIZE_ALLOW") is None:
    os.environ["APP_FILE_SIZE_ALLOW"] = str(10240*1024)

if os.environ.get("APP_USER") is None:
    os.environ["APP_USER"] = "dongvt"

if os.environ.get("APP_PASS") is None:
    os.environ["APP_PASS"] = "dongvt"


if os.environ.get("APP_DATABASE_NAME") is None:
    os.environ["APP_DATABASE_NAME"] = "file_upload"

if os.environ.get("APP_DATABASE_USER") is None:
    os.environ["APP_DATABASE_USER"] = "root"

if os.environ.get("APP_DATABASE_PASS") is None:
    os.environ["APP_DATABASE_PASS"] = ""

if os.environ.get("APP_DATABASE_HOST") is None:
    os.environ["APP_DATABASE_HOST"] = "127.0.0.1"

if os.environ.get("APP_DATABASE_PORT") is None:
    os.environ["APP_DATABASE_PORT"] = "3306"
###

ENVIRONMENT = os.environ.get("ENVIRONMENT")
DATABASE_URL = os.environ.get("DATABASE_URL")
SECRET_KEY = str(os.environ.get("APP_SECRET_KEY"))

DEBUG = True
ALLOWED_HOSTS = ["*"]

RESOURCE_UPLOAD = BASE_DIR + "/resource_upload/"

# Application definition


# handler upload file ==
FILE_TYPE_ALLOW = {
    "png": "image/png",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "pdf": "application/pdf",
    "txt": "text/plain",
    "csv": "text/plain",
    "abc": "application/octet-stream"
}

# FILE_SIZE = 10240*1024 # 10 MB
FILE_SIZE = int(os.environ.get("APP_FILE_SIZE_ALLOW"))

USER = {
    "USERNAME": str(os.environ.get("APP_USER")),
    "PASSWD": str(os.environ.get("APP_PASS"))
}
# ======================
