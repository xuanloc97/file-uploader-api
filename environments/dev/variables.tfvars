ecr_repo_name                  = "file-uploader"
ecs_cluster_name               = "app-cluster"
region                         = "us-east-1"

app_task_famliy                = "file-uploader-app-task"
container_port                 = 5000
app_task_name                  = "file-uploader-task"
ecs_task_execution_role_name   = "ecsTaskExecutionRole"

target_group_name              = "target-group"
app_service_name               = "file-uploader"
application_load_balancer_name = "file-uploader-dev"
container_memory               = 512
container_cpu                  = 256
