#!/bin/bash

source .venv/bin/activate

# Set environment variables for flask app
export FLASK_APP=main
export FLASK_ENV=development
# export FLASK_DEBUG=1

# Set python executor at project root
export PYTHONPATH="${PWD}"

# Run app
cd src && python3 main.py
