#!/bin/bash

DIRECTORY=.venv
# Install python venv
if [ -d "$DIRECTORY" ]; then
    echo "$DIRECTORY existed."
else
    python3 -m venv $DIRECTORY
fi

source .venv/bin/activate

pip install -r requirements.txt

# Install terraform
if which terraform >/dev/null; then
    echo "The terraform is installed on your system"
else
    echo "The terraform is installing ......."

    sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

    # wget -O- https://apt.releases.hashicorp.com/gpg | \
    # gpg --dearmor | \
    # sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

    # gpg --no-default-keyring \
    # --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    # --fingerprint

    # echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    # https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    # sudo tee /etc/apt/sources.list.d/hashicorp.list

    # sudo apt update
    # sudo apt-get install terraform

    wget https://releases.hashicorp.com/terraform/1.5.7/terraform_1.5.7_linux_arm64.zip
    unzip terraform_1.5.7_linux_arm64.zip
    sudo mv terraform_1.5.7_linux_arm64/terraform /usr/bin/

    echo "The terraform installed!"

fi
