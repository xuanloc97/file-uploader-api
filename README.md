# File Uploader Api
File Uploader Api is server which can view, upload, delete and reuse contain file. The server will deploy to ECS of AWS as container server.

#### Notes
We will have a url of server after terraform apply (Output will print to pipepline screen)

Currently, The server only upload on system instead of **uploading to S3** because fee on AWS Cloud

Need to update **DATABASE_URL** environment variable to can change target DB 
## Setup & Installation

Make sure you have the 3.8 version of Python installed.

```bash
git clone <repo-url>
```

```bash
cd project_root
./install_venv.sh
```

## Running The App

```bash
./run_server.sh
```

## Testing The App
### Run unit test
 
 ```bash
 source .venv/bin/activate
pytest --cov-report html:UTReport --cov=./src ./tests/unit_test
```

### Test manual app

#### On local
1. Sigup account
```
curl -X POST --location 'http://localhost:5000/signup' --form 'name="loc"' --form 'email="xuanlochcmus@gmail.com"' --form 'password="xuanlochcmus"'
```

2. Get access token
```
curl -X POST --location 'localhost:5000/login' \
--form 'email="xuanlochcmus@gmail.com"' \
--form 'password="xuanlochcmus"'
```
3. Post file

We should correct path to file 
```
curl -X POST --location 'http://localhost:5000/uploadfile' \
--header 'access-token: <access-token-here>' \
--form 'file=@"/D:/PROJECTS/test_file/test.txt"'
```
4. View file by name

You can change **test.txt**
```
curl -X GET --location 'localhost:5000/viewfile?file_name=testcopy.txt' \
--header 'access-token: <access-token-here>'
```
5. Delete file by name

You can change **test.txt**
```
curl  -X DELETE --location 'localhost:5000/uploadfile?file_name=test.txt' \
--header 'access-token: <access-token-here>>'
```
#### On ECS AWS
We will need to change url from **localhost:5000** to **file-uploader-dev-xxxxxxxx.us-east-1.elb.amazonaws.com**  and command like above steps
## Terraform:

We need to set credentials for AWS and should be private variables:
```
export TF_VAR_AWS_ACCESS_KEY_ID=XXXXXXX
export TF_VAR_AWS_SECRET_ACCESS_KEY=XXXXXX
```

We can change terraform config file on each environments:
```
nano environments/dev/variables.tfvars
``` 

### Run on local
```
cd terraform
terraform init
terraform plan --var-file=../environments/dev/variables.tfvar
terraform apply --var-file=../environments/dev/variables.tfvar
terraform destroy --var-file=../environments/dev/variables.tfvar
```
