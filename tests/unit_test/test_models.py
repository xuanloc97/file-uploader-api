from src.api.models import User, Files


def test_new_user():
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the name, email, password are defined correctly
    """
    name = 'Loc'
    email = 'xuanloc@gmail.com'
    password =  'xuanloc'
    user = User(name=name, email=email, password=password)
    assert user.name == name
    assert user.email == email
    assert user.password == password

def test_new_file():
    """
    GIVEN a file model
    WHEN a new file is created
    THEN check the name, hash are defined correctly
    """
    name = 'test.txt'
    hash =  'xuanloc'
    file = Files(name=name, hash=hash)
    assert file.name == name
    assert file.hash == hash
