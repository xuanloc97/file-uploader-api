import os
import pytest
import mock

# mock.patch("src.helpers.authentication.token_required").start()

from src.main import create_app
from src.api.models import db as _db

DB_NAME = "database.db"

basedir = os.path.abspath(os.path.dirname(__file__))

@pytest.fixture()
def app():
    """
    Create fake app to test
    """
    app = create_app()
    app.config.update({
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": f'sqlite:///{os.path.join(basedir, DB_NAME)}',
    })

    # other setup can go here

    yield app

    # clean up / reset resources here


@pytest.fixture()
def client(app):
    """
    Test config for client
    """
    with app.test_client() as client:
        yield client

@pytest.fixture()
def db(app, request):
    """Returns session-wide initialized database"""
    with app.app_context():
        _db.create_all()
        yield _db
        _db.drop_all()

# @pytest.fixture(scope="session")
# def app(request):
#     """Test session-wide test `Flask` application."""
#     app = create_app()
#     return app

# @pytest.fixture(scope="session")
# def client(app):
#     """
#     Test config for client
#     """
#     return app.test_client()

# @pytest.fixture(autouse=True)
# def _setup_app_context_for_test(request, app):
#     """
#     Given app is session-wide, sets up a app context per test to ensure that
#     app and request stack is not shared between tests.
#     """
#     ctx = app.app_context()
#     ctx.push()
#     yield  # tests will run here
#     ctx.pop()


# @pytest.fixture(scope="session")
# def db(app, request):
#     """
#     Returns session-wide initialized database
#     """
#     with app.app_context():
#         _db.create_all()
#         yield _db
#         _db.drop_all()


# @pytest.fixture(scope="function")
# def session(app, db, request):
#     """
#     Creates a new database session for each test, rolling back changes afterwards
#     """
#     connection = _db.engine.connect()
#     transaction = connection.begin()

#     options = dict(bind=connection, binds={})
#     session = _db.scoped_session(sessionmaker=options)

#     _db.session = session

#     yield session

#     transaction.rollback()
#     connection.close()
#     session.remove()
