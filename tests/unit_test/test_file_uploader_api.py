import mock
from functools import wraps

from datetime import datetime

from src.api.models import Files
from src.api.utils import handle_read_file_disk, handle_delete_file_disk


def mock_decorator(*args, **kwargs):
    """Decorate by doing nothing."""
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            return f(*args, **kwargs)
        return decorated_function
    return decorator

# mock token_required before running test cases 
mock.patch('src.helpers.authentication.token_required', mock_decorator).start()

class TestCase:
    """
    Test file_uploader_api module with test cases is listed below:
    - test_should_status_code_ok
    - test_view_files_ok
    - test_view_files_not_found
    - test_view_files_error_file_type
    - test_delete_file_type_error
    - test_delete_file_happy_path
    - test_delete_file_but_not_found
    """
    def test_should_status_code_ok(self, client):
        response = client.get('/checkhealth')
        assert response.status_code == 200

    # def test_view_files_ok(self, client, db):
    #     """
    #     Test case is that view a file successful
    #     """
    #     name = 'test.txt'
    #     hash =  'b5947d384539421d743d48a86586c7bbaaa'
    #     file = Files(name=name, hash=hash, time_modify=datetime.utcnow())
    #     db.session.add(file)
    #     db.session.commit()

    #     response = client.get(f'/viewfile?file_name={name}')
    #     assert response.status_code == 200
    #     assert response.text == 'Flask\r\nFlask-SQLAlchemy\r\nsix\r\nSerializerMixin\r\nflask-migrate==2.7.0'

    # def test_view_files_not_sync(self, client, db):
    #     """
    #     Test case is that view a file but File not sync
    #     """
    #     name = 'testnotsync.txt'
    #     hash =  'b5947d384539421d743d48a86586c7bb'
    #     file = Files(name=name, hash=hash, time_modify=datetime.utcnow())
    #     db.session.add(file)
    #     db.session.commit()

    #     response = client.get(f'/viewfile?file_name={name}')
    #     assert response.status_code == 200
    #     assert "File not sync" in response.text

    # def test_view_files_not_found(self, client):
    #     """
    #     Test case is that check file is exist on DB
    #     """
    #     response = client.get('/viewfile?file_name=testcopy.txt')
    #     assert response.status_code == 200
    #     assert "file does not exist" in response.text

    # def test_view_files_error_file_type(self, client):
    #     """
    #     Test case is that check file type not allow
    #     """
    #     response = client.get('/viewfile?file_name=tests.txtsss')
    #     assert response.status_code == 200
    #     assert "file type not allow" in response.text

    # def test_delete_file_type_error(self, client):
    #     """
    #     Test case is that check file type not allow when deleting file
    #     """
    #     response = client.delete('/uploadfile?file_name=tests.txtsss')
    #     assert response.status_code == 200
    #     assert "file type not allow" in response.text

    # @mock.patch("src.api.utils.handle_delete_file_disk")
    # def test_delete_file_happy_path(self, client, db):
    #     """
    #     Test case is that deleting file is successful
    #     """
    #     name = 'file_delete.txt'
    #     hash =  'b5947d384539421d'
    #     file = Files(name=name, hash=hash, time_modify=datetime.utcnow())
    #     files = Files.query.all()
    #     for file in files:
    #         print(file)
    #     # Add new file model
    #     db.session.add(file)
    #     db.session.commit()

        # # Mock handle delete file
        # with mock.patch("src.api.utils.handle_delete_file_disk", mock.MagicMock(return_value=(-2, name))):
        #     response = client.delete(f'/uploadfile?file_name={name}')
        #     assert response.status_code == 200
        #     assert "file type not allowaaa" in response.text

    # @mock.patch("src.api.utils.handle_delete_file_disk")
    # def test_delete_file_but_not_found(self, mock_handle_delete_file_disk, client, db):
    #     """
    #     Test case is that deleting file is not found in system
    #     """
    #     name = 'file_delete.txt'
    #     hash =  'b5947d384539421d'
    #     file = Files(name=name, hash=hash, time_modify=datetime.utcnow())
    #     # Add new file model
    #     db.session.add(file)
    #     db.session.commit()

    #     # Mock handle delete file
    #     mock_handle_delete_file_disk.return_value = (-1, name)

    #     response = client.delete('/uploadfile?file_name=file_delete')
    #     assert response.status_code == 200
    #     assert "file type not allow" in response.text
