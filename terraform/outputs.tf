output "repository_url" {
  value = module.ecrRepo.repository_url
}

output "app_url" {
  value = module.ecsCluster.app_url
}
