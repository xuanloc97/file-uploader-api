# Define all variables use in source here
variable ecr_repo_name {
  type        = string
  default     = "app-repo"
  description = "Container Registry Repository name"
}

variable ecs_cluster_name {
  type        = string
  default     = "app-cluster"
  description = "Container service cluster name"
}

variable region {
  type        = string
  default     = "us-east-1"
  description = "region where need to deploy"
}

variable app_task_famliy {
  type        = string
  default     = "tmp"
  description = "Zone where need to deploy"
}

variable container_port {
  type        = number
  default     = 5000
  description = "Container port on app"
}

variable app_task_name {
  type        = string
  default     = "file-uploader-task"
  description = "App name"
}

variable ecs_task_execution_role_name {
  type        = string
  default     = "ecsTaskExecutionRole"
  description = "ecs execution role name"
}

variable application_load_balancer_name {
  type        = string
  default     = "file-uploader-dev"
  description = "DNS naming load balancer"
}

variable target_group_name {
  type        = string
  default     = "target-group"
  description = "Target group"
}

variable app_service_name {
  type        = string
  default     = "file-uploader"
  description = "Name of server"
}

variable container_memory {
  type        = number
  default     = 512
  description = "Container memory"
}

variable container_cpu {
  type        = number
  default     = 256
  description = "Container CPU"
}

variable AWS_ACCESS_KEY_ID {
  type        = string
  description = "Access key id"
}

variable AWS_SECRET_ACCESS_KEY {
  type        = string
  description = "Secret access key"
}
