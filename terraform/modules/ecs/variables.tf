# Define variables for ecs modules
variable ecs_cluster_name {
  type        = string
  default     = "app-cluster"
  description = "Container service cluster name"
}

variable region {
  type        = string
  default     = "us-east-1"
  description = "Region where need to deploy"
}

variable app_task_famliy {
  type        = string
  default     = "file-uploader-app-task"
  description = "Naming server of app"
}

variable ecr_repo_url {
  type        = string
  description = "Url of ECR repo"
}

variable container_port {
  type        = number
  default     = 5000
  description = "Container port on app"
}

variable app_task_name {
  type        = string
  default     = "file-uploader-task"
  description = "App task name"
}

variable ecs_task_execution_role_name {
  type        = string
  default     = "ecsTaskExecutionRole"
  description = "ecs execution role name"
}

variable application_load_balancer_name {
  type        = string
  default     = "file-uploader-dev"
  description = "DNS naming load balancer"
}

variable target_group_name {
  type        = string
  default     = "target-group"
  description = "Target group"
}

variable app_service_name {
  type        = string
  default     = "file-uploader"
  description = "Name of server"
}

variable container_memory {
  type        = number
  default     = 512
  description = "Container memory"
}

variable container_cpu {
  type        = number
  default     = 256
  description = "Container CPU"
}
