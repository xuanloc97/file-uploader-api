resource "aws_ecr_repository" "app_ecr_repo" {
  name = var.ecr_repo_name #
  force_delete = true  # Still delete if have images
  
## should uncomment below block to scan image to more security
#   image_scanning_configuration {
#     scan_on_push = true
#   }

}
