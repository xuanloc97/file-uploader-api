# Define variables for ecr modules
variable ecr_repo_name {
  type        = string
  default     = "file-uploader"
  description = "Container Registry Repository name"
}
