# Set providers
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.45.0"
    }
  }
}

provider "aws" {
  region  = var.region # The region where environment is going to be deployed # Use your own region here
  access_key = var.AWS_ACCESS_KEY_ID # Enter AWS IAM 
  secret_key = var.AWS_SECRET_ACCESS_KEY # Enter AWS IAM 
}
