# Module create ECR 
module "ecrRepo" {
  source = "./modules/ecr"

  ecr_repo_name = var.ecr_repo_name
}

# Module create ECS
module "ecsCluster" {
  source = "./modules/ecs"

  region                         = var.region
  ecs_cluster_name               = var.ecs_cluster_name

  app_task_famliy                = var.app_task_famliy
  ecr_repo_url                   = module.ecrRepo.repository_url
  container_port                 = var.container_port
  app_task_name                  = var.app_task_name
  ecs_task_execution_role_name   = var.ecs_task_execution_role_name

  target_group_name              = var.target_group_name
  app_service_name               = var.app_service_name
  application_load_balancer_name = var.application_load_balancer_name

  container_memory               = var.container_memory
  container_cpu                  = var.container_cpu
}
